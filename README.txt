--STATUS--
In progress.
Todo:
	- Continuous code cleanup and optimization
	- Triple fusion scripts
	- Party fusion scripts + routing + interface
	- List all data + interface
	
--BACKGROUND--
This is a tool for the game Shin Megami Tensei (SNES), in which you collect so-called demons that help you in battle. You can fuse these demons in pairs
or trios, and get entirely new demons as a result. The result is dictated by fairly complex rules and conditions dealing with the demons' different qualities.
The game is quite old and has a clunky and slow interface, which makes keeping track of possible fusion combinations a tedious task. This tool is designed
to assist gameplay by making it easier to plan your fusions before executing them.

--PROJECT SUMMARY--
Frontend is pure React and custom CSS. (client folder)
Backend is a simple Node.js/Express server.
The fusion calculations are done with Python scripts (scripts folder), which are accessed by Express with the Python Shell module (routes folder).
Data is received in React as JSON for further processing.