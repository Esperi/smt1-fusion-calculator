import React, { Component } from 'react';
import './App.css';
import Content from './components/content.js';
import Header from './components/header.js';
import Nav from './components/nav.js';

class App extends Component {
  state = {demon: "", activelink: "#fusetwo"}

  navCallback = (dataFromNav) => {
    this.setState({ activelink: dataFromNav });
  }

  render() {
    return (
      <div className="App">
        <Header/>
        <Nav callback={this.navCallback} />
        <Content showncomponent={this.state.activelink} />
        <p className="copyright">SMT(SNES) Fusion Calculator by LP 2018</p>
      </div>
    );
  }
}

export default App;