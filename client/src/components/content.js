import React, { Component } from 'react';
import '../App.css';
import Fusetwo from './fusetwo.js';
import Fusethree from './fusethree.js';
import Result from './result.js';

class Content extends Component {

    constructor(props) {
        super(props);
        this.state = {demon1: '', demon2: '', demon3: '', showncomponent: null, demonlist: null, prevcontent: null};
        this.fetchDemons = this.fetchDemons.bind(this);
        this.setNewContent = this.setNewContent.bind(this);
    }   

    fetchDemons() {
        fetch('/fusion?demoninput1=&demoninput2=&task=list')
        .then(res => res.json())
        .then(data => {
            var demonstring = data[0]
            var demonjson = JSON.parse(demonstring)
            this.setState({
              demonlist: demonjson
            })
        });
    }

    componentDidMount() {
        this.fetchDemons();
        this.setState({ showncomponent: this.props.showncomponent })
    }

    setNewContent() {
        this.setState({ showncomponent: this.props.showncomponent})
    }

    static getDerivedStateFromProps(props, state) {
        if (state.prevcontent !== props.showncomponent) {
            return {
                prevcontent: props.showncomponent
            }
        }
        return null;
    }  
    
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.showncomponent !== this.props.showncomponent) {
            this.setNewContent();
        }
    }

    demonCallback1 = (dataFromForm1) => {
        this.setState({ demon1: dataFromForm1 });
    }

    demonCallback2 = (dataFromForm2) => {
        this.setState({ demon2: dataFromForm2 });
    }

    demonCallback3 = (dataFromForm3) => {
        this.setState({ demon3: dataFromForm3 });
    }

    render() {
        let chosencontent = null
        if (this.state.demonlist !== null) {
            if (this.state.showncomponent === null || this.state.showncomponent === "#fusetwo") {
                chosencontent = <Fusetwo callback1={this.demonCallback1} callback2={this.demonCallback2} demonlist={this.state.demonlist} />
            } else if (this.state.showncomponent === "#triplefusion") {
                chosencontent = <Fusethree callback1={this.demonCallback1} callback2={this.demonCallback2} callback3={this.demonCallback3} demonlist={this.state.demonlist} />
            }
        }
        return (
            <div className="content">
                <div className="motherContent">
                    <div className="content1">
                        {chosencontent}
                    </div>
                    <div className="content2">
                        <Result demon1={this.state.demon1} demon2={this.state.demon2} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Content;