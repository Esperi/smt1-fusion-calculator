import React, { Component } from 'react';
import '../App.css';

class Filters extends Component {

    constructor(props) {
        super(props);
        this.state = {
            demonlist: null
        };
        this.handleChange = this.handleChange.bind(this);
        this.getRaces = this.getRaces.bind(this);
        this.mapRaces = this.mapRaces.bind(this);
    }

    componentWillMount() {
        this.setState({ demonlist: this.props.demonlist })
    }

    getRaces() {
        var data = this.state.demonlist
        var races = []
        for (var i in data) {
            if (races.indexOf(data[i][2]) === -1) {
                races.push(data[i][2])
            }
        }
        races = races.sort()
        return races;
    }

    mapRaces(races,int) {
        const listoptions = () => {
            return races.map((raceoption) =>
                <option key={raceoption} value={raceoption}>
                    {raceoption}
                </option>
            )
        }

        if (int === 1) {
            return (
                <select name="raceFilter1" defaultValue="All" value={this.state.value} onChange={this.handleChange} className="miniSelect">
                    <option value="All">All</option>
                    {listoptions()}
                </select>
            );
        } else if (int === 2) {
            return (
                <select name="raceFilter2" defaultValue="All" value={this.state.value} onChange={this.handleChange} className="miniSelect">
                    <option value="All">All</option>
                    {listoptions()}
                </select>
            );
        } else if (int === 3) {
            return (
                <select name="raceFilter3" defaultValue="All" value={this.state.value} onChange={this.handleChange} className="miniSelect">
                    <option value="All">All</option>
                    {listoptions()}
                </select>
            );
        }
    }

    handleChange(event) {
        var filtername = event.target.name
        var filter = event.target.value
        if (filtername === "raceFilter1") {
            this.props.filterCallback1(filter)
        } else if (filtername === "raceFilter2") {
            this.props.filterCallback2(filter)
        } else if (filtername === "raceFilter3") {
            this.props.filterCallback3(filter)
        }
    }

    render() {

        if(this.state.demonlist === null)
            return null;
        
        let raceoptions1 = this.mapRaces(this.getRaces(),1)
        let raceoptions2 = this.mapRaces(this.getRaces(),2)
        let raceoptions3 = null
        if (this.props.filteramount === 3) {
            raceoptions3 = this.mapRaces(this.getRaces(),3)
        } else {
            raceoptions3 = null
        }

        return (
            <div className="filters">
                <form>
                    <label className="filterLabel">
                    Filter by race<br/>
                    {raceoptions1}
                    {raceoptions2}
                    {raceoptions3}
                    </label>
                </form>
            </div>
        )
    }
}

export default Filters;