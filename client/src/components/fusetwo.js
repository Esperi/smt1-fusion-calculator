import React, { Component } from 'react';
import '../App.css';
import Filters from './filters.js'

class Fusetwo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            demon1: null,
            demon2: null,
            demonlist: null,
            raceFilter1: 'All',
            raceFilter2: 'All',
            filteramount: 2
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.mapDemons = this.mapDemons.bind(this);
        this.findRaceDemons1 = this.findRaceDemons1.bind(this);
        this.findRaceDemons2 = this.findRaceDemons2.bind(this);
        this.renderSubmitButton = this.renderSubmitButton.bind(this);
    }

    componentWillMount() {
        this.setState({ demonlist: this.props.demonlist })
    }

    findRaceDemons1() {
        var data = this.state.demonlist
        var racelist1 = []
        var key = null
        for (key in data) {
            if (data[key][2] === this.state.raceFilter1) {
                racelist1.push(key)
            }
        }
        racelist1 = racelist1.sort()
        return racelist1;
    }

    findRaceDemons2() {
        var data = this.state.demonlist
        var racelist2 = []
        var key = null
        for (key in data) {
            if (data[key][2] === this.state.raceFilter2) {
                racelist2.push(key)
            }
        }
        racelist2 = racelist2.sort()
        return racelist2;
    }

    mapDemons(int) {
        var data = Object.getOwnPropertyNames(this.state.demonlist)
        data = data.sort()
        var racelist1 = []
        var racelist2 = []
        
        const listoptions1 = () => {
            if (this.state.raceFilter1 === 'All') {
                return data.map((demonoption) => 
                    <option key={demonoption} value={demonoption}>
                        {demonoption}
                    </option>
                )
            } else if (this.state.raceFilter1 !== 'All') {
                racelist1 = this.findRaceDemons1()
                return racelist1.map((racedemon) => 
                    <option key={racedemon} value={racedemon}>
                        {racedemon}
                    </option>
                )
            }
        }

        const listoptions2 = () => {
            if (this.state.raceFilter2 === 'All') {
                return data.map((demonoption) => 
                    <option key={demonoption} value={demonoption}>
                        {demonoption}
                    </option>
                )
            } else if (this.state.raceFilter2 !== 'All') {
                racelist2 = this.findRaceDemons2()
                return racelist2.map((racedemon) => 
                    <option key={racedemon} value={racedemon}>
                        {racedemon}
                    </option>
                )
            }
        }

        if (int === 1) {
            return (
                <select name="demon1" value={this.state.value} onChange={this.handleChange} required>
                    <option value="select" defaultValue>SELECT</option>
                    {listoptions1()}
                </select>
            );
        } else if (int === 2) {
            return (
                <select name="demon2" value={this.state.value} onChange={this.handleChange} required>
                    <option value="select" defaultValue>SELECT</option>
                    {listoptions2()}
                </select>
            );
        }
    }

    handleChange(event) {
        var selectname = event.target.name
        if (selectname === "demon1") {
            this.setState({
                demon1: event.target.value
            });
        } else if (selectname === "demon2") {
            this.setState({
                demon2: event.target.value
            });
        }
    }

    handleSubmit(event) {
        this.props.callback1(this.state.demon1);
        this.props.callback2(this.state.demon2);
        event.preventDefault();
    }

    filterCallback1 = (dataFromFilter) => {
        this.setState({ raceFilter1: dataFromFilter, demon1: "select" });
    }

    filterCallback2 = (dataFromFilter) => {
        this.setState({ raceFilter2: dataFromFilter, demon2: "select" });
    }

    renderSubmitButton() {
        if (this.state.demon1 === null || this.state.demon2 === null || this.state.demon1 === "select" || this.state.demon2 === "select") {
            return (
                <input type="submit" value="FUSE" className="fusebutton" disabled />
            )
        } else {
            return (
                <input type="submit" value="FUSE" className="fusebutton" />
            )
        }
    }

    render() {

        if(this.state.demonlist === null)
            return (
                <div className="fusioninput">
                    <p>LOADING ..</p>
                </div>
            );
        
        let options1 = this.mapDemons(1)
        let options2 = this.mapDemons(2)
        let submitbutton = this.renderSubmitButton()

        return (
            <div className="fusioninput">
                <Filters demonlist={this.state.demonlist} filterCallback1={this.filterCallback1} filterCallback2={this.filterCallback2} filteramount={this.state.filteramount} />
                <form onSubmit={this.handleSubmit}>
                    <label>
                        FIRST DEMON
                        {options1}
                        <br/>
                    </label>
                    <label>
                        SECOND DEMON
                        {options2}<br/>
                    </label>
                    <input type="hidden" name="task" value="fuse" />
                    {submitbutton}
                </form>
            </div>
        )
    }
}

export default Fusetwo;