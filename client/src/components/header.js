import React, { Component } from 'react';
import '../App.css';

class Header extends Component {

    render() {
        return (
            <div className="header">
                <img src={require('../resources/header.jpg')} alt="Shin Megami Tensei (SNES) Fusion Calculator" />
            </div>
        )
    }
}

export default Header;