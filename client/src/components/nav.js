import React, { Component } from 'react';
import '../App.css';

class Nav extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activelink: "#fusetwo",
            previousactive: "#triplefusion",
            navlinks: ["Fusion", "Triple fusion", "Party fusion", "All demons"],
            navvalues: ["#fusetwo", "#triplefusion", "#partyfusion", "#all"],
            navclassnames: ["navlink active", "navlink", "navlink", "navlink"]
        };

        this.handleclick = this.handleclick.bind(this);
        this.renderNav = this.renderNav.bind(this);
        this.generateActiveState = this.generateActiveState.bind(this);
    }

    handleclick(event) {
        var clicked = event.target.getAttribute('value')
        this.setState({ previousactive: this.state.activelink, activelink: clicked }, () => { this.generateActiveState() })
    }

    generateActiveState() {
        var i = 0
        var tempclassnames = ["navlink", "navlink", "navlink", "navlink"]
        while (i < this.state.navvalues.length) {
            if (this.state.navvalues[i] === this.state.activelink) {
                tempclassnames[i] = "navlink active"
            }
            i = i+1
        }
        this.setState({ navclassnames: tempclassnames }, () => { this.props.callback(this.state.activelink) });
    }

    renderNav() {
        var navlinks = this.state.navlinks
        var navvalues = this.state.navvalues
        var navclassnames = this.state.navclassnames

        const navoptions = () => {
            return navlinks.map((navlink, index) =>
                <li key={navlink} className={navclassnames[index]}><a href={navvalues[index]} value={navvalues[index]} onClick={this.handleclick}>{navlink}</a></li>
            )
        }

        return(
            <ul>
                {navoptions()}
            </ul>
        )
    }

    render() {
        let navoptions = this.renderNav()
        return (
            <div className="navBar">
                {navoptions}
            </div>
        )
    }
}

export default Nav;