import React, { Component } from 'react';
import '../App.css';

class Result extends Component {

    constructor(props) {
        super(props);
        this.state = {
            demon1: null,
            demon2: null,
            resultdemon: 'DEMON',
            level: '####',
            family: '####',
            race: '####',
            alignment: '####'
        };

        this.fetchResult = this.fetchResult.bind(this);
    }

    componentDidMount() {
        this.setState({ demon1: this.props.demon1, demon2: this.props.demon2 })
    }

    static getDerivedStateFromProps(props, state) {
        if (state.prevdemon1 !== props.demon1 || state.prevdemon2 !== props.demon2) {
            return {
                prevdemon1: props.demon1,
                prevdemon2: props.demon2
            }
        }
        return null;
    }  
    
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.demon1 !== this.props.demon1 || prevProps.demon2 !== this.props.demon2) {
            this.fetchResult();
        }
    }

    fetchResult() {
        fetch('/fusion?demoninput1=' + this.props.demon1 + '&demoninput2=' + this.props.demon2 + '&task=fuse')
        .then(res => res.json())
        .then(data => {
            var demonstring = data[0]
            var demonjson = JSON.parse(demonstring)
          //document.getElementById('output').innerHTML = JSON.stringify(data)
          this.setState({
              resultdemon: demonjson.demon,
              level: (demonjson.level).toString(),
              family: demonjson.family,
              race: demonjson.race,
              alignment: demonjson.alignment
          }, () => {console.log(demonjson)})
        });
    }

    render() {

        var demonname = this.state.resultdemon

        if (demonname.indexOf(' ') !== -1) {
            demonname = demonname.replace(' ', '_')
        }

        return (
            <div className="result">
            <p className="fusetext">{this.props.demon1} X {this.props.demon2}</p>
            <img src={require('../resources/sprites/' + demonname + '.png')} alt='Result demon sprite' className="sprite" />
                <table>
                    <thead>
                        <tr>
                            <th colSpan="2" id="resultblink">{this.state.resultdemon}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="headline">FAMILY</td>
                            <td className="headline">RACE</td>
                        </tr>
                        <tr>
                            <td>{this.state.family}</td>
                            <td>{this.state.race}</td>
                        </tr>
                        <tr>
                            <td className="headline">LEVEL</td>
                            <td className="headline">ALIGNMENT</td>
                        </tr>
                        <tr>
                            <td>{this.state.level}</td>
                            <td>{this.state.alignment}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Result;