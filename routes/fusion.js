var express = require('express'),
    bodyparser = require('body-parser'),
    app = express(),
    router = express.Router();
    PythonShell = require('python-shell');

    app.use(function(req, res, next) {
        app.use(bodyparser.urlencoded({ extended: false }))
        app.use(bodyparser.json())
        return next();
    });

router.get('/', calldemonfusion);

function calldemonfusion(req, res) {
    var options = {
        mode: 'text',
        pythonOptions: ['-u'],
        scriptPath: './scripts/',
        args:
            [
                req.query.demoninput1,
                req.query.demoninput2,
                req.query.task
            ]
    }

    PythonShell.run('demonfusionprogram.py', options, function(err, results) {
        if (err) throw err;
        res.send(results)
    });
}

module.exports = router;