import csv
import normalfusion
from collections import defaultdict
import sys
import os
import json

demondict = defaultdict(list)
name = ''
#resultdemon = None
#demoninput1 = 'Ose'
#demoninput2 = 'Dwarf'
demoninput1 = str(sys.argv[1])
demoninput2 = str(sys.argv[2])
task = str(sys.argv[3])
#task = 'fuse'

##cwd = os.getcwd()
##print(cwd)
##print(os.listdir(cwd))

with open('./demonlist.csv', 'r') as csvlist:
    linereader = csv.reader(csvlist, delimiter=',', quotechar='|')
    for line in linereader:
        name = line[0]
        demondict[name].append(int(line[1]))
        demondict[name].append(line[2])
        demondict[name].append(line[3])
        demondict[name].append(line[4])

def fuse(demondict,demoninput1,demoninput2):
    results = normalfusion.fusetwo(demondict,demoninput1,demoninput2)
    resultstr = str(results)
    resultstr = resultstr.replace("'",'"')
    print(resultstr)

def listall(demondict):
    demonsstr = str(demondict)
    demonsstr = demonsstr.replace("'",'"')
    demonsstr = demonsstr[:-1]
    demonsstr = demonsstr[28:]
    print(demonsstr)

if (task == 'fuse'):
    fuse(demondict,demoninput1,demoninput2)
elif (task == 'list'):
    listall(demondict)
