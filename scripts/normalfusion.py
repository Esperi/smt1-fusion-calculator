from collections import defaultdict

def fusetwo(demondict,demoninput1,demoninput2):

    ##demoninput1 = input('Eka demoni: ')
    ##demoninput2 = input('Toka demoni: ')

    demon1 = demondict.get(demoninput1)
    demon2 = demondict.get(demoninput2)

    family1 = demon1[1]
    family2 = demon2[1]
    race1 = demon1[2]
    race2 = demon2[2]
    level1 = demon1[0]
    level2 = demon2[0]
    alignment1 = demon1[3]
    alignment2 = demon2[3]

    resultdemon = ''
    message = ''
    resultrace = ''
    resultlevel = -0
    demon3 = None
    fusiontype = None

    resultlevel = (level1+level2)/2+2

    if (demoninput1 == 'Slime' or demoninput2 == 'Slime'):
        if (demoninput1 == 'Slime'):
            resultdemon = demoninput2
        else:
            resultdemon = demoninput1

    elif ((demoninput1 == 'Rangda' and demoninput2 == 'Barong') or \
        (demoninput1 == 'Barong' and demoninput2 == 'Rangda')):
        resultdemon = 'Shiva'

    elif ((race1 == 'Therian' and race2 == 'Therian') or \
          (race1 == 'Deity' and race2 == 'Deity') or \
          (race1 == 'Megami' and race2 == 'Megami') or \
          (race1 == 'Tenma' and race2 == 'Tenma') or \
          (race1 == 'Kishin' and race2 == 'Kishin') or \
          (race1 == 'Seraph' and race2 == 'Seraph') or \
          (race1 == 'Dragon' and race2 == 'Dragon')):
        resultdemon = 'Invalid fusion'

    elif (race1 == 'Element' or race2 == 'Element'):
        if ('Dark' in alignment1 or 'Dark' in alignment2):
            resultdemon = 'Invalid fusion'
        elif (family1 == 'God' or family2 == 'God'):
            resultdemon = 'Invalid fusion'
        else:
            resultdemon = fuseelemental(demoninput1,demoninput2,demon1,demon2,demondict,race1,race2,family1,family2,level1,level2)

    elif (race1 == 'Messian' or race2 == 'Messian'):
        resultdemon = 'Random Law-aligned demon'

    elif (race1 == 'Gaean' or race2 == 'Gaean'):
        resultdemon = 'Random Chaos-aligned demon'

    elif ('Dark' in alignment1 or 'Dark' in alignment2):
        resultdemon = darkfusion(demon1,demon2,demondict,race1,race2,level1,level2,alignment1,alignment2)
           
    elif (race1 == race2):
        resultdemon = checkelemental(demon1,demon2,demondict,race1)

    elif (family1 == family2):
        resultdemon = familyfusion(demon1,demon2,demondict,family1)

    else:
        resultrace = predictrace(demon1,demon2,demondict,race1,race2)

        if (resultrace != 'Invalid fusion'):
            resultlevel = (level1 + level2)/2+2
            fusiontype = 'normal'
            resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

    if (resultrace == 'Invalid fusion' or resultdemon == 'Invalid fusion'):
        message = 'Invalid fusion'
        results = returnexception(message)
    elif (resultdemon == 'Random Law-aligned demon' or resultdemon == 'Random Chaos-aligned demon'):
        message = resultdemon
        results = returnexception(message)
    elif (resultdemon == '' or resultdemon == None):
        message = 'Error'
        results = returnexception(message)
    else:
        result = demondict.get(resultdemon)
        results = {
            "demon": resultdemon,
            "level": result[0],
            "family": result[1],
            "race": result[2],
            "alignment": result[3]
        }
    
    return (results)

def returnexception(message):

    results = {
        "demon": message,
        "level": "none",
        "family": "none",
        "race": "none",
        "alignment": "none"
    }

    return (results)

def darkfusion(demon1,demon2,demondict,race1,race2,level1,level2,alignment1,alignment2):

    averagelevel = (level1 + level2) / 2
    resultlevel = averagelevel + 2
    resultdemon = None
    resultrace = None
    passdemon = None
    fusiontype = None

    if ('Dark' in alignment1 and 'Dark' in alignment2):

        if (race1 == race2):

            if (race1 == 'Vile'):
                #print('Vile rankdown')
                
                if (level1 > level2):
                    passdemon = demon1
                else:
                    passdemon = demon2

                resultrace = 'Vile'
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)
                return resultdemon
                    
            elif (race1 == 'Tyrant'):
                resultdemon = 'Invalid fusion'
                return resultdemon

            elif (race1 == 'Drake'):
                resultrace = 'Dragon'

            elif (race1 == 'Raptor'):
                resultrace = 'Avian'

            elif (race1 == 'Wilder'):
                resultrace = 'Holy'

            elif (race1 == 'Jaki'):
                resultrace = 'Touki'

            elif (race1 == 'Ghost'):
                resultrace = 'Spirit'

            elif (race1 == 'Spirit'):
                resultrace = 'Undead'

            elif (race1 == 'Undead'):
                resultrace = 'Foul'

            elif (race1 == 'Foul'):
                resultrace = 'Fairy'

            fusiontype = 'normal'
            resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)
            return resultdemon
            
        elif (race1 != race2):
            #print('Two dark different race')
            #print('Higher level demon rankdown')

            if (level1 > level2):
                passdemon = demon1
            else:
                passdemon = demon2

            fusiontype = 'rankdown'
            resultrace = passdemon[2]
            resultlevel = rankdown(passdemon,demondict)
            resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)
            return resultdemon

    elif (('Dark' in alignment1 and level1 > level2) or \
          ('Dark' in alignment2 and level2 > level1)):
        
        #print('Dark demon level higher than normal demon')

        if (averagelevel % 3 == 0):
            #print('Dark demon rankup')

            if ('Dark' in alignment1):
                passdemon = demon1
            else:
                passdemon = demon2

            fusiontype = 'rankup'
            resultrace = passdemon[2]
            resultlevel = rankup(passdemon,demondict,fusiontype)
            resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)
            return resultdemon
            
        else:
            resultdemon = 'Slime'
            return resultdemon

    elif (('Dark' in alignment1 and level2 > level1) or \
          ('Dark' in alignment2 and level1 > level2)):
        
        #print('Normal demon level higher than dark demon')

        if (averagelevel % 7 == 0):
            #print('Normal demon rankup')

            if ('Dark' in alignment1):
                passdemon = demon2
            else:
                passdemon = demon1

            fusiontype = 'rankup'
            resultrace = passdemon[2]
            resultlevel = rankup(passdemon,demondict,fusiontype)
            resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)
            return resultdemon
            
        else:
            resultdemon = 'Slime'
            return resultdemon
        
    else:
        print('Dark fusion error')

def rankup(passdemon,demondict,fusiontype):

    race = passdemon[2]
    level = passdemon[0]
    resultlevel = -1
    racedict = defaultdict(list)
    levellist = []
    resultdemon = None
    i = 0
    levellistlength = -1

    for key,value in demondict.items():
        if (value[2] == race):
            racedict[key].append(int(value[0]))
            levellist.append(int(value[0]))

    levellistlength = len(levellist)
    
    while (i < levellistlength):
        if (levellist[i] == level):
            if (fusiontype == 'rankuptwice'):
                if ((i-2) == -1):
                    resultlevel = levellist[levellistlength-1]
                elif ((i-2) == -2):
                    resultlevel = levellist[levellistlength-2]
                else:
                    resultlevel = levellist[i-2]
            else:
                if ((i-1) == -1):
                    resultlevel = levellist[levellistlength-1]
                else:
                    resultlevel = levellist[i-1]
        i = i + 1

    return resultlevel

def rankdown(passdemon,demondict):

    race = passdemon[2]
    level = passdemon[0]
    resultlevel = -1
    racedict = defaultdict(list)
    levellist = []
    resultdemon = None
    i = 0
    levellistlength = -1

    for key,value in demondict.items():
        if (value[2] == race):
            racedict[key].append(int(value[0]))
            levellist.append(int(value[0]))

    levellistlength = len(levellist)
    
    while (i < levellistlength):
        if (levellist[i] == level):
            if (i+2 > levellistlength):
                resultlevel = levellist[0]
            else:
                resultlevel = levellist[i+1]
        i = i + 1

    return resultlevel
    

def getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype):

    racedict = defaultdict(list)
    levellist = []
    resultdemon = None

    for key,value in demondict.items():
        if (value[2] == resultrace):
            racedict[key].append(int(value[0]))
            levellist.append(int(value[0]))

    if (fusiontype == 'rankup' or fusiontype == 'rankuptwice' or fusiontype == 'rankdown'):
        for key,value in racedict.items():
            if(value[0] == resultlevel):
                resultdemon = key
        return resultdemon

    else:
        nearestlevel = getnearestlevel(levellist,resultlevel)
        for key,value in racedict.items():
            if(value[0] == nearestlevel):
                if (level1 == nearestlevel or level2 == nearestlevel):
                    resultdemon = 'Slime'
                else:
                    resultdemon = key
        return resultdemon


def getnearestlevel(levellist,resultlevel):

    levellist = sorted(levellist)
    levellistlength = len(levellist)
    pienin = -1
    i = 0
    nearestlevel = -1

    while(i < levellistlength):
        if (pienin < 0):
            pienin = 0
        else:
            if (abs(levellist[i] - resultlevel) < abs(levellist[pienin] - resultlevel)):
                pienin = i
        i = i+1
    
    if (levellist[pienin] < resultlevel):
        if ((pienin+2) > levellistlength):
            nearestlevel = levellist[pienin]
        else:
            nearestlevel = levellist[pienin+1]
    else:
        nearestlevel = levellist[pienin]
    
    return nearestlevel

def familyfusion(demon1,demon2,demondict,family1):

    familydict = defaultdict(list)
    level1 = demon1[0]
    level2 = demon2[0]
    resultlevel = (level1 + level2)/2+3
    levellist = []
    nearestlvl = 0
    resultdemon = None

    for key,value in demondict.items():
        if (value[1] == family1):
            familydict[key].append(int(value[0]))
            levellist.append(int(value[0]))

    nearestlevel = getnearestlevel(levellist,resultlevel)

    for key,value in familydict.items():
        if(value[0] == nearestlevel):
            if (level1 == nearestlevel or level2 == nearestlevel):
                resultdemon = 'Slime'
            else:
                resultdemon = key
            return resultdemon

def fuseelemental(demoninput1,demoninput2,demon1,demon2,demondict,race1,race2,family1,family2,level1,level2):

    resultdemon = None
    passdemon = None
    resultlevel = None
    resultrace = None
    fusiontype = None

    if (family1 == family2):

        if (demoninput1 == demoninput2):
            resultdemon = 'Invalid fusion'

        elif (demoninput1 == 'Erthys' or demoninput2 == 'Erthys'):
            
            if (demoninput1 == 'Aeros' or demoninput2 == 'Aeros'):
                resultdemon = 'Bugaboo'

            elif (demoninput1 == 'Aquans' or demoninput2 == 'Aquans'):
                resultdemon = 'Bogle'

            elif (demoninput1 == 'Flaemis' or demoninput2 == 'Flaemis'):
                resultdemon = 'Unicorn'

            elif (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
                resultdemon = 'Worm'

            elif (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                resultdemon = 'Onkot'

            elif (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                resultdemon = 'Dryad'

            elif (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Berith'

        elif (demoninput1 == 'Aeros' or demoninput2 == 'Aeros'):

            if (demoninput1 == 'Aquans' or demoninput2 == 'Aquans'):
                resultdemon = 'Jack Frost'

            elif (demoninput1 == 'Flaemis' or demoninput2 == 'Flaemis'):
                resultdemon = 'Sytry'

            elif (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
                resultdemon = 'Wyrm'

            elif (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                resultdemon = 'Furiae'

            elif (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                resultdemon = 'Gandharva'

            elif (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Phoenix'

        elif (demoninput1 == 'Aquans' or demoninput2 == 'Aquans'):

            if (demoninput1 == 'Flaemis' or demoninput2 == 'Flaemis'):
                resultdemon = 'Bai Ze'

            elif (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
                resultdemon = 'Qilin'

            elif (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                resultdemon = 'Siren'

            elif (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                resultdemon = 'Forneus'

            elif (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Orobas'

        elif (demoninput1 == 'Flaemis' or demoninput2 == 'Flaemis'):

            if (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
                resultdemon = 'Dwarf'

            elif (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                resultdemon = 'Feng Huang'

            elif (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                resultdemon = 'Yaksini'

            elif (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Lakhe'

        elif (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
            
            if (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                resultdemon = 'Angel'

            elif (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                resultdemon = 'Raja Naga'

            elif (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Kotosirunusi'

        elif (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
            
            if (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                resultdemon = 'Archangel'

            elif (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Agni'
                
        elif (demoninput1 == 'Undine' or demoninput2 == 'Undine'):
            if (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                resultdemon = 'Itzamna'

    else:

        if (race1 == 'Tenma' or race2 == 'Tenma'):
            resultdemon = 'Invalid fusion'

        elif ((race1 == 'Kishin' or race2 == 'Kishin') or \
              (race1 == 'Femme' or race2 == 'Femme')):
            if (race1 == 'Kishin' or race2 == 'Kishin'):
                resultrace = 'Kishin'
                if (race1 == 'Kishin'):
                    passdemon = demon1
                else:
                    passdemon = demon2
            else:
                resultrace = 'Femme'
                if (race1 == 'Femme'):
                    passdemon = demon1
                else:
                    passdemon = demon2
            
            if ((demoninput1 == 'Undine' or demoninput2 == 'Undine') or \
                 (demoninput1 == 'Salamander' or demoninput2 == 'Salamander')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)
            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Yoma' or race2 == 'Yoma'):
            resultrace = 'Yoma'
            if (race1 == 'Yoma'):
                passdemon = demon1
            else:
                passdemon = demon2

            if (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Erthys' or demoninput2 == 'Erthys') or \
                 (demoninput1 == 'Aeros' or demoninput2 == 'Aeros') or \
                 (demoninput1 == 'Undine' or demoninput2 == 'Undine') or \
                 (demoninput1 == 'Salamander' or demoninput2 == 'Salamander')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Night' or race2 == 'Night'):
            resultrace = 'Night'
            if (race1 == 'Night'):
                passdemon = demon1
            else:
                passdemon = demon2
                
            if (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Sylph' or demoninput2 == 'Sylph') or \
                 (demoninput1 == 'Undine' or demoninput2 == 'Undine')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Fairy' or race2 == 'Fairy'):
            resultrace = 'Fairy'
            if (race1 == 'Fairy'):
                passdemon = demon1
            else:
                passdemon = demon2
                
            if ((demoninput1 == 'Sylph' or demoninput2 == 'Sylph') or \
                (demoninput1 == 'Undine' or demoninput2 == 'Undine')):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Aeros' or demoninput2 == 'Aeros') or \
                 (demoninput1 == 'Aquans' or demoninput2 == 'Aquans') or \
                 (demoninput1 == 'Gnome' or demoninput2 == 'Gnome')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif ((race1 == 'Divine' or race2 == 'Divine') or \
              (race1 == 'Snake' or race2 == 'Snake')):
            if (race1 == 'Divine' or race2 == 'Divine'):
                resultrace = 'Divine'
                if (race1 == 'Divine'):
                    passdemon = demon1
                else:
                    passdemon = demon2
            else:
                resultrace = 'Snake'
                if (race1 == 'Snake'):
                    passdemon = demon1
                else:
                    passdemon = demon2

            if((demoninput1 == 'Sylph' or demoninput2 == 'Sylph') or \
               (demoninput1 == 'Undine' or demoninput2 == 'Undine') or \
               (demoninput1 == 'Gnome' or demoninput2 == 'Gnome') or \
               (demoninput1 == 'Salamander' or demoninput2 == 'Salamander')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Fallen' or race2 == 'Fallen'):
            resultrace = 'Fallen'
            if (race1 == 'Fallen'):
                passdemon = demon1
            else:
                passdemon = demon2
                
            if (demoninput1 == 'Salamander' or demoninput2 == 'Salamander'):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Erthys' or demoninput2 == 'Erthys') or \
                 (demoninput1 == 'Gnome' or demoninput2 == 'Gnome')):
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Dragon' or race2 == 'Dragon'):
            resultrace = 'Dragon'
            if (race1 == 'Dragon'):
                passdemon = demon1
            else:
                passdemon = demon2

            if((demoninput1 == 'Undine' or demoninput2 == 'Undine') or \
                 (demoninput1 == 'Salamander' or demoninput2 == 'Salamander')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif ((race1 == 'Avian' or race2 == 'Avian') or \
              (race1 == 'Flight' or race2 == 'Flight')):
            if (race1 == 'Avian' or race2 == 'Avian'):
                resultrace = 'Avian'
                if (race1 == 'Avian'):
                    passdemon = demon1
                else:
                    passdemon = demon2
            else:
                resultrace = 'Flight'
                if (race1 == 'Flight'):
                    passdemon = demon1
                else:
                    passdemon = demon2

            if (demoninput1 == 'Sylph' or demoninput2 == 'Sylph'):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Aeros' or demoninput2 == 'Aeros') or \
               (demoninput1 == 'Salamander' or demoninput2 == 'Salamander')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Avatar'
            if (race1 == 'Avatar'):
                passdemon = demon1
            else:
                passdemon = demon2

            if((demoninput1 == 'Flaemis' or demoninput2 == 'Flaemis') or \
               (demoninput1 == 'Salamander' or demoninput2 == 'Salamander') or \
               (demoninput1 == 'Gnome' or demoninput2 == 'Gnome')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif ((race1 == 'Holy' or race2 == 'Holy') or \
              (race1 == 'Beast' or race2 == 'Beast')):
            if (race1 == 'Holy' or race2 == 'Holy'):
                resultrace = 'Holy'
                if (race1 == 'Holy'):
                    passdemon = demon1
                else:
                    passdemon = demon2
            else:
                resultrace = 'Beast'
                if (race1 == 'Beast'):
                    passdemon = demon1
                else:
                    passdemon = demon2

            if((demoninput1 == 'Flaemis' or demoninput2 == 'Flaemis') or \
               (demoninput1 == 'Salamander' or demoninput2 == 'Salamander') or \
               (demoninput1 == 'Gnome' or demoninput2 == 'Gnome') or \
               (demoninput1 == 'Undine' or demoninput2 == 'Undine')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Touki'
            if (race1 == 'Touki'):
                passdemon = demon1
            else:
                passdemon = demon2

            if (demoninput1 == 'Gnome' or demoninput2 == 'Gnome'):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Undine' or demoninput2 == 'Undine') or \
               (demoninput1 == 'Salamander' or demoninput2 == 'Salamander') or \
               (demoninput1 == 'Erthys' or demoninput2 == 'Erthys')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Brute'
            if (race1 == 'Brute'):
                passdemon = demon1
            else:
                passdemon = demon2

            if ((demoninput1 == 'Gnome' or demoninput2 == 'Gnome') or \
                (demoninput1 == 'Undine' or demoninput2 == 'Undine')):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif((demoninput1 == 'Aquans' or demoninput2 == 'Aquans') or \
               (demoninput1 == 'Erthys' or demoninput2 == 'Erthys')):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Jirae'
            if (race1 == 'Jirae'):
                passdemon = demon1
            else:
                passdemon = demon2

            if ((demoninput1 == 'Gnome' or demoninput2 == 'Gnome') or \
                (demoninput1 == 'Erthys' or demoninput2 == 'Erthys')):
                fusiontype = 'rankuptwice'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            elif(demoninput1 == 'Undine' or demoninput2 == 'Undine'):
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Therian'
            if (race1 == 'Therian'):
                passdemon = demon1
            else:
                passdemon = demon2

            if((demoninput1 == 'Erthys' or demoninput2 == 'Erthys') or \
               (demoninput1 == 'Aeros' or demoninput2 == 'Aeros')):
                fusiontype = 'rankdown'
                resultlevel = rankdown(passdemon,demondict)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

            else:
                fusiontype = 'rankup'
                resultlevel = rankup(passdemon,demondict,fusiontype)
                resultdemon = getdemon(resultrace,resultlevel,demondict,level1,level2,fusiontype)

    return resultdemon

def checkelemental(demon1,demon2,demondict,race1):

    resultdemon = ''
    
    if (race1 == 'Femme' or race1 == 'Snake'):
        resultdemon = 'Undine'
        
    elif (race1 == 'Yoma' or race1 == 'Flight'):
        resultdemon = 'Aeros'

    elif (race1 == 'Night' or race1 == 'Jirae'):
        resultdemon = 'Erthys'

    elif (race1 == 'Fairy'):
        resultdemon = 'Aquans'

    elif (race1 == 'Divine' or race1 == 'Avian'):
        resultdemon = 'Sylph'

    elif (race1 == 'Fallen' or race1 == 'Holy' or race1 == 'Beast'):
        resultdemon = 'Flaemis'

    elif (race1 == 'Avatar'):
        resultdemon = 'Salamander'

    elif (race1 == 'Touki' or race1 == 'Brute'):
        resultdemon = 'Gnome'

    else:
        resultdemon = 'Invalid fusion'

    return resultdemon


def predictrace(demon1,demon2,demondict,race1,race2):

    resultrace = ''

    if (race1 == 'Deity' or race2 == 'Deity'):

        if ((race1 == 'Femme' or race2 == 'Femme') or \
           (race1 == 'Yoma' or race2 == 'Yoma') or \
           (race1 == 'Divine' or race2 == 'Divine')):
            resultrace = 'Megami'

        elif (race1 == 'Night' or race2 == 'Night'):
            resultrace = 'Yoma'
            
        elif (race1 == 'Fairy' or race2 == 'Fairy'):
            resultrace = 'Night'

        elif ((race1 == race2) or \
             (race1 == 'Tenma' or race2 == 'Tenma') or \
             (race1 == 'Kishin' or race2 == 'Kishin') or \
             (race1 == 'Seraph' or race2 == 'Seraph')):
            resultrace = 'Invalid fusion'

        elif ((race1 == 'Fallen' or race2 == 'Fallen') or \
             (race1 == 'Dragon' or race2 == 'Dragon')):
            resultrace= 'Tenma'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
             (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Kishin'

        elif (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Divine'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Fallen'

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Dragon'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Snake'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Avatar'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Brute'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Beast'

    elif (race1 == 'Megami' or race2 == 'Megami'):

        if ((race1 == race2) or \
            (race1 == 'Tenma' or race2 == 'Tenma') or \
            (race1 == 'Kishin' or race2 == 'Kishin') or \
            (race1 == 'Seraph' or race2 == 'Seraph')):
            resultrace = 'Invalid fusion'

        elif (race1 == 'Femme' or race2 == 'Femme'):
            resultrace = 'Fairy'

        elif ((race1 == 'Yoma' or race2 == 'Yoma') or \
              (race1 == 'Fallen' or race2 == 'Fallen')):
            resultrace = 'Tenma'

        elif (race1 == 'Night' or race2 == 'Night'):
            resultrace = 'Jirae'

        elif (race1 == 'Fairy' or race2 == 'Fairy'):
            resultrace = 'Yoma'

        elif ((race1 == 'Divine' or race2 == 'Divine') or \
              (race1 == 'Flight' or race2 == 'Flight')):
            resultrace = 'Fallen'

        elif((race1 == 'Dragon' or race2 == 'Dragon') or \
             (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Femme'

        elif((race1 == 'Snake' or race2 == 'Snake') or \
             (race1 == 'Avatar' or race2 == 'Avatar')):
            resultrace = 'Dragon'
        
        elif (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Divine'

        elif ((race1 == 'Holy' or race2 == 'Holy') or \
              (race1 == 'Therian' or race2 == 'Therian')):
            resultrace = 'Snake'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Avatar'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Brute'

    elif (race1 == 'Tenma' or race2 == 'Tenma'):

        if (race1 == 'Seraph' or race2 == 'Seraph'):
            resultrace = 'Invalid fusion'
            
        elif (race1 == 'Yoma' or race2 == 'Yoma'):
            resultrace = 'Megami'

        elif ((race1 == 'Night' or race2 == 'Night') or \
              (race1 == 'Flight' or race2 == 'Flight')):
            resultrace = 'Fallen'

        elif ((race1 == 'Fairy' or race2 == 'Fairy') or \
              (race1 == 'Fallen' or race2 == 'Fallen')):
            resultrace = 'Yoma'

        elif (race1 == 'Divine' or race2 == 'Divine'):
            resultrace = 'Foul'

        elif ((race1 == 'Dragon' or race2 == 'Dragon') or \
              (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Kishin'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
              (race1 == 'Avatar' or race2 == 'Avatar')):
            resultrace = 'Dragon'

        elif (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Divine'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Snake'
            
        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Avatar'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Brute'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Drake'

    elif (race1 == 'Kishin' or race2 == 'Kishin'):

        if ((race1 == 'Yoma' or race2 == 'Yoma') or \
            (race1 == 'Fairy' or race2 == 'Fairy') or \
            (race1 == 'Divine' or race2  == 'Divine') or \
            (race1 == 'Flight' or race2 == 'Flight')):
            resultrace = 'Femme'

        elif (race1 == 'Night' or race2 == 'Night'):
            resultrace = 'Fallen'

        elif ((race1 == 'Seraph' or race2 == 'Seraph') or \
             (race1 == 'Dragon' or race2 == 'Dragon')):
            resultrace = 'Tenma'

        elif (race1 == 'Fallen' or race2 == 'Fallen'):
            resultrace = 'Night'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
             (race1 == 'Avatar' or race2 == 'Avatar') or \
             (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Dragon'

        elif ((race1 == 'Avian' or race2 == 'Avian') or \
             (race1 == 'Holy' or race2 == 'Holy') or \
             (race1 == 'Brute' or race2 == 'Brute')):
            resultrace = 'Snake'

        elif ((race1 == 'Beast' or race2 == 'Beast') or \
              (race1 == 'Therian' or race2 == 'Therian')):
            resultrace = 'Avatar'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Touki'

    elif (race1 == 'Femme' or race2 == 'Femme'):

        if ((race1 == 'Yoma' or race2 == 'Yoma') or \
            (race1 == 'Fairy' or race2 == 'Fairy')):
            resultrace = 'Night'

        elif (race1 == 'Night' or race2 == 'Night'):
            resultrace = 'Fallen'

        elif ((race1 == 'Seraph' or race2 == 'Seraph') or \
              (race1 == 'Divine' or race2 == 'Divine') or \
              (race1 == 'Snake' or race2 == 'Snake')):
            resultrace = 'Megami'

        elif ((race1 == 'Brute' or race2 == 'Brute') or \
            (race1 == 'Fallen' or race2 == 'Fallen')):
            resultrace = 'Touki'

        elif ((race1 == 'Dragon' or race2 == 'Dragon') or \
            (race1 == 'Avatar' or race2 == 'Avatar')):
            resultrace = 'Kishin'

        elif ((race1 == 'Avian' or race2 == 'Avian') or \
            (race1 == 'Jirae' or race2 == 'Jirae')):
            resultrace = 'Brute'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Yoma'

        elif ((race1 == 'Holy' or race2 == 'Holy') or \
            (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Snake'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Avatar'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Drake'

    elif (race1 == 'Yoma' or race2 == 'Yoma'):

        if (race1 == 'Seraph' or race2 == 'Seraph'):
            resultrace = 'Deity'

        elif ((race1 == 'Divine' or race2 == 'Divine') or \
              (race1 == 'Flight' or race2 == 'Flight')):
            resultrace = 'Fallen'

        elif (race1 == 'Fallen' or race2 == 'Fallen'):
            resultrace = 'Night'

        elif ((race1 == 'Dragon' or race2 == 'Dragon') or \
              (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Femme'

        elif (race1 == 'Snake' or race2 == 'Snake'):
            resultrace = 'Avian'       

        elif (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Snake'

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Therian'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Drake'

        elif ((race1 == 'Beast' or race2 == 'Beast') or \
              (race1 == 'Jirae' or race2 == 'Jirae')):
            resultrace = 'Brute'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Beast'

    elif (race1 == 'Night' or race2 == 'Night'):

        if (race1 == 'Seraph' or race2 == 'Seraph'):
            resultrace = 'Deity'

        elif (race1 == 'Divine' or race2 == 'Divine'):
            resultrace = 'Fallen'

        elif (race1 == 'Fallen' or race2 == 'Fallen'):
            resultrace = 'Yoma'     

        elif (race1 == 'Dragon' or race2 == 'Dragon'):
            resultrace = 'Snake'

        elif (race1 == 'Snake' or race2 == 'Snake'):
            resultrace = 'Drake'

        elif ((race1 == 'Avian' or race2 == 'Avian') or \
              (race1 == 'Flight' or race2 == 'Flight')):
            resultrace = 'Raptor'

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Beast'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Therian'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Wilder'

        elif (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Femme'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Jaki'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Spirit'
            
        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Ghost'

    elif (race1 == 'Fairy' or race2 == 'Fairy'):

        if ((race1 == 'Avian' or race2 == 'Avian') or \
              (race1 == 'Seraph' or race2 == 'Seraph')):
            resultrace = 'Divine'

        elif (race1 == 'Divine' or race2 == 'Divine'):
            resultrace = 'Megami'

        elif (race1 == 'Dragon' or race2 == 'Dragon'):
            resultrace = 'Avatar'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
              (race1 == 'Beast' or race2 == 'Beast')):
            resultrace = 'Holy'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Fallen'

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Snake'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Beast'

        elif (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Femme'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Yoma'

        elif ((race1 == 'Jirae' or race2 == 'Jirae') or \
              (race1 == 'Therian' or race2 == 'Therian')):
            resultrace = 'Brute'

    elif (race1 == 'Seraph' or race2 == 'Seraph'):

        if (race1 == 'Dragon' or race2 == 'Dragon'):
            resultrace = 'Tenma'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
              (race1 == 'Avatar' or race2 == 'Avatar')):
            resultrace = 'Dragon'

        elif (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Megami'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Avian'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Avatar'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Holy'

        elif ((race1 == 'Touki' or race2 == 'Touki') or \
              (race1 == 'Brute' or race2 == 'Brute')):
            resultrace = 'Yoma'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Fairy'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Brute'

    elif (race1 == 'Divine' or race2 == 'Divine'):

        if ((race1 == 'Touki' or race2 == 'Touki') or \
            (race1 == 'Brute' or race2 == 'Brute') or \
            (race1 == 'Fallen' or race2 == 'Fallen')):
            resultrace = 'Yoma'

        elif (race1 == 'Dragon' or race2 == 'Dragon'):
            resultrace = 'Tenma'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
              (race1 == 'Beast' or race2 == 'Beast')):
            resultrace = 'Holy'

        elif (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Megami'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Avian'

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Dragon'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Avatar'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Holy'
            
        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Fairy'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Brute'

    elif (race1 == 'Fallen' or race2 == 'Fallen'):

        if (race1 == 'Dragon' or race2 == 'Dragon'):
            resultrace = 'Drake'

        elif ((race1 == 'Snake' or race2 == 'Snake') or \
              (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Femme'

        elif ((race1 == 'Avian' or race2 == 'Avian') or \
              (race1 == 'Avatar' or race2 == 'Avatar')):
            resultrace = 'Yoma'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Night'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Beast'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Therian'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Ghost'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Jaki'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Wilder'

    elif (race1 == 'Dragon' or race2 == 'Dragon'):

        if ((race1 == 'Avian' or race2 == 'Avian') or \
              (race1 == 'Therian' or race2 == 'Therian')):
            resultrace = 'Tenma'

        elif ((race1 == 'Flight' or race2 == 'Flight') or \
              (race1 == 'Touki' or race2 == 'Touki')):
            resultrace = 'Kishin'

        elif ((race1 == 'Avatar' or race2 == 'Avatar') or \
              (race1 == 'Holy' or race2 == 'Holy') or \
              (race1 == 'Beast' or race2 == 'Beast')):
            resultrace = 'Snake'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Drake'

    elif (race1 == 'Snake' or race2 == 'Snake'):

        if (race1 == 'Avian' or race2 == 'Avian'):
            resultrace = 'Dragon'

        elif (race1 == 'Flight' or race2 == 'Flight'):
            resultrace = 'Femme'

        elif (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Avian'
            
        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Avatar'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Holy'

        elif (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Kishin'

        elif (race1 == 'Brue' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Brute'
            
        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Beast'

    elif (race1 == 'Avian' or race2 == 'Avian'):

        if (race1 == 'Avatar' or race2 == 'Avatar'):
            resultrace = 'Dragon'

        elif (race1 == 'Holy' or race2 == 'Holy'):
            resultrace = 'Avatar'

        elif (race1 == 'Beast' or race2 == 'Beast'):
            resultrace = 'Holy'

        elif (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Avatar'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Night'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Wilder'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Beast'

    elif (race1 == 'Flight' or race2 == 'Flight'):

        if ((race1 == 'Avatar' or race2 == 'Avatar') or \
              (race1 == 'Holy' or race2 == 'Holy')):
            resultrace = 'Avian'

        elif ((race1 == 'Beast' or race2 == 'Beast') or \
              (race1 == 'Therian' or race2 == 'Therian')):
            resultrace = 'Brute'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Fallen'

        elif (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Femme'

        elif (race1 == 'Jirae' or race2 == 'Jirae'):
            resultrace = 'Raptor'

    elif (race1 == 'Avatar' or race2 == 'Avatar'):

        if (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Kishin'

        elif ((race1 == 'Brute' or race2 == 'Brute') or \
              (race1 == 'Therian' or race2 == 'Therian') or \
              (race1 == 'Jirae' or race2 == 'Jirae')):
            resultrace = 'Snake'

    elif (race1 == 'Holy' or race2 == 'Holy'):

        if (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Avatar'

        elif (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Wilder'

        elif ((race1 == 'Brute' or race2 == 'Brute') or \
              (race1 == 'Jirae' or race2 == 'Jirae')):
            resultrace = 'Beast'

    elif (race1 == 'Beast' or race2 == 'Beast'):

        if (race1 == 'Touki' or race2 == 'Touki'):
            resultrace = 'Therian'

        elif (race1 == 'Brute' or race2 == 'Brute'):
            resultrace = 'Touki'

        elif ((race1 == 'Therian' or race2 == 'Therian') or \
              (race1 == 'Jirae' or race2 == 'Jirae')):
            resultrace = 'Holy'

    elif (race1 == 'Touki' or race2 == 'Touki'):

        if (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Kishin'

    elif (race1 == 'Brute' or race2 == 'Brute'):

        if (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Yoma'

    elif (race1 == 'Jirae' or race2 == 'Jirae'):

        if (race1 == 'Therian' or race2 == 'Therian'):
            resultrace = 'Fairy'

    return resultrace
